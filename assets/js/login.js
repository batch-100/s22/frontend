let loginForm = document.querySelector("#logInUser");

//addeventlistener to the form
//(e) meaning when it is triggered, it will create an event object
loginForm.addEventListener("submit", (e)=> {
	e.preventDefault();
	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	//console.log(email);
	//console.log(password);

	if (email == "" || password == "") {
		alert("Please input your email and/or password.");
	}else{
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if (data.accessToken) {
				//store jwt on local storage
				localStorage.setItem('token', data.accessToken)
				//send fetch request to decode  JWT  and obtain  user ID  and role  for  starting the context
				fetch(`http://localhost:4000/api/users/details`, {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					//set the global user to have properties  containing  authenticated user's IS and role
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					window.location.replace("./courses.html")

				})
			}else {
				alert("Something went Wrong");
			}
		})
	}

});
