let adminUser = localStorage.getItem("isAdmin");

//cardfooter will be dynamically rendered if the use is an admin or not
let cardFooter;
//fetch
fetch('http://localhost:4000/api/courses')
.then(res => res.json())
.then(data => {
	//log the data to check if you were able to fetch the data  from the  server 
	console.log(data)

	let courseData;

	if(data.length < 1) {
		courseData = "No courses available."
	} else {
		//else iterate the courses collection
		courseData = data.map(course => {
			//to check the make up of each element inside the  courses collection
			console.log(course._id)

			//if hte user is regular user
			if(adminUser == "false" || !adminUser) {
				cardFooter =
				`
					<a href="./course.html?courseId = ${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">Select Course</a>

				`
			}else{
				cardFooter = 
				`
					<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton" value="${course._id}">Edit Course</a>

					<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block dangerButton" value="${course._id}">Delete Course</a>


				`
			}
			return(
				`
					<div class = "col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">${course.price}</p>

							</div>
							<div class="card-footer">
								${cardFooter}

							</div>
						</div>
					</div>

				`
				)
		}).join("");
	}
	let container = document.querySelector("#coursesContainer")
	//get the value of courseData and assign it  as the #courseContainer's content
	container.innerHTML = courseData;
})

//add modal button
let modalButton = document.querySelector("#adminButton")

if(adminUser =="false" || !adminUser) {
	modalButton.innerHTML = null
} else {
	//display add course
	modalButton.innerHTML = 
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse" value="" class="btn btn-block btn-primary">Add Course</a>


		</div>	


	`
}